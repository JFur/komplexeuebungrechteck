package controller;
import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;

public class BunteRechteckeController {
	
	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	
	public void BunteRechteckController() {
		rechtecke = new LinkedList<Rechteck>();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}
	
	public void add(Rechteck rechteck) {
	 rechtecke.add(rechteck);	
	 this.database.rechteck_eintragen(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> linkedlist = new LinkedList<String>();
		linkedlist.add("Item1");
	    linkedlist.add("Item5");
	    linkedlist.add("Item3");
	    linkedlist.add("Item6");
	    linkedlist.add("Item2");
	}
}
