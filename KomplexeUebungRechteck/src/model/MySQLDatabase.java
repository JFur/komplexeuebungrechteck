package model;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.sun.jndi.ldap.Connection;

public class MySQLDatabase {
private final String driver = "com.mysql.jubc.Driver";
private final String url = "jdbc:mysql://localhost/rechtecke?";
private final String user = "root";
private final String password = "";

	public void rechteck_eintragen(Rechteck r) {
	try {
			Class.forName(driver);
			
			connection con = DriverManager.getConnection(url),user,password;
			String sql = "insert into T_Rechtecke (x,y,breite, hoehe) values\r\n" + 
					"(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			con.close();
			
	}catch (Exception e) {
		e.printStackTrace();
	}
}


	public List<Rechteck> getAlleRechtecke() {
	List rechtecke = new LinkedList<Rechteck>();
	try {
		Class.forName(driver);
		
		java.sql.Connection con = DriverManager.getConnection(url,user,password);
		String sql = "SELECT FROM T_Rechtecke";
		Statement s = con.createStatement();
		ResultSet rs.executeQuery(sql);
			while(rs.next()) {
			int x = rs.getInt("x");
			int y = rs.getInt("y");
			int breite = rs.getInt("breite");
			int hoehe = rs.getInt("hoehe");
			rechtecke.add(new Rechteck(x, y, breite, hoehe));
			}
		
			con.close();
	}catch (Exception e) {
		e.printStackTrace();
	}	
	return rechtecke;
	}
}

