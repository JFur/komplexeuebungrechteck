package view;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;

public class BunteRechteckGui extends JFrame {

	private Zeichenflaeche contentPane;
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuItemNeuesRechteck;
	private BunteRechteckeController brc;
	//private JTextField textField;
	//private JLabel lblNewLabel_1;
	//private JTextField textField_1;
	//private JLabel lblNewLabel_2;
	//private JTextField textField_2;
	//private JLabel lblNewLabel_3;
	//private JTextField textField_3;
	//private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BunteRechteckGui frame = new BunteRechteckGui();
					frame.setVisible(true);
					frame.run();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public void rechteckhinzufuegen() {
		new RechteckNeuGui(this);
	}
	
	protected void run() {
		// TODO Auto-generated method stub
		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGui() {
		this.brc = new BunteRechteckeController();
		
		
	
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1980, 1024);
		contentPane = new Zeichenflaeche(this.brc);
		//contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.menubar = new JMenuBar();
		setJMenuBar(menubar);
		this.menu = new JMenu("Hinzufügen");
		this.menubar.add(menu);
		this.menuItemNeuesRechteck = new JMenuItem("Rechteck hinzufügen");
		this.menu.add(this.menuItemNeuesRechteck);
		this.menuItemNeuesRechteck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				menuItemNeuesRechteck_Clicked();
				this.brc.rechteckhinzufuegen();
			}});
		
		setVisible(true);
		
		
		//contentPane.setLayout(new GridLayout(0, 2, 0, 0));
		
		//JLabel lblNewLabel = new JLabel("New label");
		//contentPane.add(lblNewLabel);
		
		//textField = new JTextField();
		//contentPane.add(textField);
		//textField.setColumns(10);
		
		//lblNewLabel_1 = new JLabel("New label");
		//contentPane.add(lblNewLabel_1);
		
		//textField_1 = new JTextField();
		//contentPane.add(textField_1);
		//textField_1.setColumns(10);
		
		//lblNewLabel_2 = new JLabel("New label");
		//contentPane.add(lblNewLabel_2);
		
		//textField_2 = new JTextField();
		//contentPane.add(textField_2);
		//textField_2.setColumns(10);
		
		//lblNewLabel_3 = new JLabel("New label");
		//contentPane.add(lblNewLabel_3);
		
		//textField_3 = new JTextField();
		//contentPane.add(textField_3);
		//textField_3.setColumns(10);
		
		//btnNewButton = new JButton("New button");
		//contentPane.add(btnNewButton);
	}

	protected void menuItemNeuesRechteck_Clicked() {
		// TODO Auto-generated method stub
		
	}

}
