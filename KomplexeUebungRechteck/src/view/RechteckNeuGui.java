package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class RechteckNeuGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckNeuGui frame = new RechteckNeuGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckNeuGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	protected void buttonSpeichernClicked() {
		int x = Integer.parseInt(tfdX.getText());
		int y = Integer.parseInt(tfdY.getText());
		int laenge = Integer.parseInt(tfdLaenge.getText());
		int hoehe = Integer.parseInt(tfdHoehe.getText());
		Rechteck r = new Rechteck(x,y, laenge, hoehe);
		brc.add(r);
	}
	
}
